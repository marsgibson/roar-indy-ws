# Copyright 2023 michael. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

costmap_node_manager:
  ros__parameters:
    auto_start_trigger_topic: /carla/ego_vehicle/laserscan
    use_sim_time: True
    
local_costmap:
  local_costmap:
    ros__parameters:
      update_frequency: 30.0
      publish_frequency: 30.0
      global_frame: ego_vehicle
      robot_base_frame: ego_vehicle
      use_sim_time: True
      rolling_window: true
      width: 50
      height: 20
      resolution: 0.5
      footprint_padding: 1.0
      footprint: "[[-1.0, -1.0], [-1.0, 1.0], [1.0, 1.0], [2.5, 0.0], [1.0, -1.0]]"

      plugins: ["obstacle_layer", "voxel_layer", "inflation_layer"]
      
      obstacle_layer:
        plugin: "nav2_costmap_2d::ObstacleLayer"
        enabled: True
        observation_sources: scan
        footprint_clearing_enabled: true
        max_obstacle_height: 2.0
        combination_method: 1
        scan:
          topic: /carla/ego_vehicle/laserscan
          max_obstacle_height: 100.0
          min_obstacle_height: 0.5
          obstacle_max_range: 100.0
          obstacle_min_range: 0.0
          obstacle_range: 200.0
          raytrace_range: 200.0
          clearing: True
          marking: True
          data_type: "LaserScan"
          inf_is_valid: true
          static_map: false
          
      inflation_layer:
        plugin: "nav2_costmap_2d::InflationLayer"
        cost_scaling_factor: 3.0
        inflation_radius: 0.55
      voxel_layer:
        plugin: "nav2_costmap_2d::VoxelLayer"
        enabled: True
        publish_voxel_map: True
        origin_z: 0.0
        z_resolution: 0.05
        z_voxels: 16
        max_obstacle_height: 2.0
        mark_threshold: 0
        observation_sources: scan
        scan:
          topic: /carla/ego_vehicle/laserscan
          max_obstacle_height: 2.0
          clearing: True
          marking: True
          data_type: "LaserScan"
          max_obstacle_height: 100.0
          min_obstacle_height: 0.1
          obstacle_max_range: 100.0
          obstacle_min_range: 0.0
          obstacle_range: 200.0
          raytrace_range: 200.0
          raytrace_max_range: 300
          raytrace_min_range: 0.0
      static_layer:
        map_subscribe_transient_local: True
      always_send_full_costmap: True
  local_costmap_client:
    ros__parameters:
      use_sim_time: True
  local_costmap_rclcpp_node:
    ros__parameters:
      use_sim_time: True

global_costmap:
  global_costmap:
    ros__parameters:
      width: 10000
      height: 10000
      origin_x: -5000.0
      origin_y: -5000.0
      update_frequency: 5.0
      publish_frequency: 5.0
      global_frame: map
      robot_base_frame: ego_vehicle
      use_sim_time: True
      robot_radius: 1.0
      resolution: 2.0
      track_unknown_space: true
      plugins: ["obstacle_layer", "inflation_layer"]
      obstacle_layer:
        plugin: "nav2_costmap_2d::ObstacleLayer"
        enabled: True
        observation_sources: scan
        scan:
          topic: /carla/ego_vehicle/laserscan
          max_obstacle_height: 100.0
          min_obstacle_height: 0.1
          obstacle_max_range: 100.0
          obstacle_min_range: 0.0
          obstacle_range: 200.0
          raytrace_range: 200.0
          raytrace_max_range: 300
          raytrace_min_range: 0.0
          clearing: True
          marking: True
          data_type: "LaserScan"
      static_layer:
        plugin: "nav2_costmap_2d::StaticLayer"
        map_subscribe_transient_local: True
      inflation_layer:
        plugin: "nav2_costmap_2d::InflationLayer"
        cost_scaling_factor: 3.0
        inflation_radius: 0.55
      always_send_full_costmap: True
  global_costmap_client:
    ros__parameters:
      use_sim_time: True
  global_costmap_rclcpp_node:
    ros__parameters:
      use_sim_time: True

